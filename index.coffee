module.exports = class DPClient

	constructor: (@host = 'delayping.herokuapp.com', @port = 80) ->
		null

	get: (id, callback) ->
		@_send 'GET', id, false, callback

	create: (body, callback) ->
		@_send 'POST', null, body, callback

	update: (id, body, callback) ->
		@_send 'PUT', id, body, callback

	remove: (id, callback) ->
		@_send 'DELETE', id, false, callback

	_send: (method, path, body, callback) ->
		opts =
			hostname: @host,
			port: @port,
			path: '/queue'
			method: method
		opts.path = opts.path + '/' + path if path

		http = require 'http'
		req = http.request opts, (res) ->
			data = ''
			res.on 'data', (chunk) -> data += chunk
			res.on 'end', () ->
				try
					return callback null, JSON.parse(data), opts
				catch e
					return callback e, data, opts
			res.on 'error', (err) -> return callback error

		req.write JSON.stringify(body) if body
		req.end()
		return req
